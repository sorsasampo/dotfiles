" https://github.com/junegunn/vim-plug/wiki/tips#automatic-installation
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()

Plug 'airblade/vim-gitgutter'
" {{{
set updatetime=100
" }}}
Plug 'jceb/vim-orgmode'
Plug 'easymotion/vim-easymotion'
Plug 'fatih/vim-go'
" {{{
" https://github.com/fatih/vim-go/issues/207
let g:go_fmt_command = "goimports"
augroup vim-go
  autocmd!
  autocmd FileType go nmap <leader>b <Plug>(go-build)
  autocmd FileType go nmap <leader>r <Plug>(go-run)
  autocmd FileType go nmap <leader>t <Plug>(go-test)
augroup END
" }}}
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
" {{{
nnoremap <c-p> :FZF<cr>
" }}}
Plug 'junegunn/seoul256.vim'
Plug 'python-mode/python-mode', { 'branch': 'develop' }
" {{{
let g:pymode_lint_ignore = ["E501",]
" }}}
Plug 'tmux-plugins/vim-tmux-focus-events'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-obsession'
Plug 'dhruvasagar/vim-prosession' " depends on tpope/vim-obsession
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-vinegar'

call plug#end()

colorscheme seoul256
set hlsearch
set list
set noswapfile
set wmh=0

" vim: set foldmethod=marker:

Repository for my personal dotfiles, eg. configuration files for various Linux
programs.

    git clone https://gitlab.com/sorsasampo/dotfiles.git ~/.dotfiles

[GNU stow][stow] is used for installing dotfiles for each application.

For example, to install configuration for `tmux` and `vim`:

    cd ~/.dotfiles
    stow -v tmux vim

[stow]: https://www.gnu.org/software/stow/
